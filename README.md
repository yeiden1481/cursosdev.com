# Curso de Python y Odoo + facturación electrónica 
***
*Author:* facturaloperu.com  
*Created:* 23/03/2019
*Version:* 1.0  

***
## Table of Contents
1. **Introducción a Python Odoo y facturación electrónica, Python I**
2. **Python II**
3. **Odoo I**
4. **Odoo II**
5. **Proyecto**

***
## 1. Introducción a Python Odoo y facturacién electrónica & Python I:

* Introducción y Demo
* Estructuras lógicas en python
* Operadores
* Condiciones
* Repetitivas
* Listas
* Ejercicios.

### Recursos:

+ Transmisión en vivo:

    - <a href="http://www.youtube.com/watch?feature=player_embedded&v=lil4s0Gm23Q
        " target="_blank"><img src="http://img.youtube.com/vi/lil4s0Gm23Q/0.jpg" 
        alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>

+ [Cheat sheet python 3](https://drive.google.com/file/d/18PdrqZ9RVXfUhHQIn392PuBlkus381mC/view?usp=sharing)

+ [Repl.it](https://repl.it/languages/python3)

+ [snakify](https://snakify.org/es/lessons/print_input_numbers/)

+ Demo ODOO:
    - URL : http://facturaloperuonline.com:8069
    - User : odoo@odoo.com    
    - Pass : odoo

+ Demo API Facturador SUNAT:
    - URL : http://odoo.facturaloperuonline.com/documents
    - User : odoo@gmail.com   
    - Pass : 123456

+ Repositorio API:
    - URL : https://gitlab.com/eriquegasparcarlos/apifacturalopro

+ Repositorio Plugin Odoo:
    - URL : https://gitlab.com/rash07/facturaloperu_api_invoice

+ Ejerccios:
    - 1.- **Input, print and numbers**
        -   Suma de tres números.
        ``` python
            # This program reads two numbers and prints their sum:
            a = int(input())
            b = int(input())
            print(a + b)

            # Can you change it so it can add three numbers?
        ```
        -   Escriba un programa que salude a la persona imprimiendo la palabra 'Hola' y el nombre de la persona.
        -   Escribe un programa que tome un número e imprima su cuadrado.

    - 2.- **Integer and float numbers**
        -   Dado un número entero, imprime su último dígito.
        -   Dado un entero Imprime sus diez dígitos.

    - 3.- **Conditions: if, then, else**
        -   Dado dos enteros, imprime el valor más pequeño.
        -   Dado tres enteros, imprime el valor más pequeño

    - 4.- **For loop with range**
        -   Dados dos enteros A y B (A ≤ B). Imprime todos los números de A a B inclusive.
        -   Dados dos enteros A y B. Imprima todos los números de A a B inclusive, en orden ascendente, si A u003cB, o en orden descendente, si A ≥ B

    - 5.- **Strings**
        -   Dada una cadena formada por palabras separadas por espacios. Determina cuántas palabras tiene. Para resolver el problema, usa el método count.
        -   Dada una cadena que consiste exactamente en dos palabras separadas por un espacio. Imprima una nueva cadena con las posiciones de la primera y la segunda palabra intercambiadas (la segunda palabra se imprime primero).rn Esta tarea no debe usar bucles y si.

    - 6.- **While loop**
        -   Para un entero N dado, imprima todos los cuadrados de números enteros donde el cuadrado es menor o igual que N, en orden ascendente.
        -   Dado un número entero no inferior a 2. Imprima su divisor entero más pequeño mayor que 1.

    - 7.- **Lists**
        -   Dada una lista de números, busque e imprima todos los elementos de la lista con un número de índice par. (es decir, A [0], A [2], A [4], ...).
        -   Dada una lista de números, encuentre e imprima todos los elementos que sean un número par. En este caso, use un bucle for que recorra la lista, ¡y no sus índices! Es decir, no usar rango ()

***
## 2. Python II:
 
* Objetos y clases
* Colecciones
* Funciones y recursividad
* Generadores
* Mini proyecto con python

+ Ejerccios:
    - 1.- **Clases y objetos**
        -   Escriba una clase calculadora que permita sumar y multiplicar 2 numeros cualesquiera ingresados previamente.

    - 2.- **Funciones y recursividad**
        -   Escriba una función para el factorial de un número con y sin recursividad.

 ***
## 3. Odoo I:
 
* Instroducción a Docker
* Docker imagenes hub, file, compose, network
* Docker hub
* Docker file
* Docker compose
* Docker network
* Instalación y desplique de Odoo con docker en entornos Linux y windows
* Integración Odoo + API facturación electrónica

+ Creación de módulos:
    - 1.- https://www.odoo.com/documentation/11.0/howtos/backend.html
        
 ***
## 4. Odoo II:
 
* Creación de modulos
* Estructura de un módulo, MVC
* Modelos
* ORM
* menues
* formularios listas y búsquedas
 ***

## 5. Proyecto:
 
* Desarrollar interfaces en Odoo para integración con el API REST
de notas de crédito y débito, consumo de web services






